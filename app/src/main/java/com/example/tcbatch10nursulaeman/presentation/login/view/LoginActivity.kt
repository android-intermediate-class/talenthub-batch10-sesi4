package com.example.tcbatch10nursulaeman.presentation.login.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.tcbatch10nursulaeman.R
import com.example.tcbatch10nursulaeman.databinding.ActivityLoginBinding
import com.example.tcbatch10nursulaeman.di.AppModule
import com.example.tcbatch10nursulaeman.di.DaggerAppComponent
import com.example.tcbatch10nursulaeman.presentation.home.view.HomeActivity
import com.example.tcbatch10nursulaeman.presentation.login.viewmodel.LoginViewModel
import com.example.tcbatch10nursulaeman.utils.extension.onClick
import java.util.regex.Pattern
import javax.inject.Inject

class LoginActivity : AppCompatActivity() {

    private var _layout: ActivityLoginBinding? = null

    private val layout: ActivityLoginBinding
        get() = _layout ?: throw IllegalStateException("The activity has been destroyed")

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var mLoginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityLoginBinding.inflate(layoutInflater)
        _layout = binding
        setContentView(binding.root)

        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build().inject(this)

        mLoginViewModel = ViewModelProvider(
            this,
            viewModelFactory
        )[LoginViewModel::class.java]

        layout.tieEmail.addTextChangedListener(
            afterTextChanged = {
                validateEmailPassword(it.toString(), binding.tiePassword.text.toString())
            })

        layout.tiePassword.addTextChangedListener(
            afterTextChanged = {
                validateEmailPassword(binding.tieEmail.text.toString(), it.toString())
            })

        layout.tieEmail.setText("Bob@mail.com")
        layout.tiePassword.setText("12345678")

        layout.btnLogin.onClick {
            mLoginViewModel.saveUserEmail(layout.tieEmail.text.toString())
        }

        mLoginViewModel.saveUserState.observe(this, Observer {
            if (it == "success") {
                val intent = Intent(this, HomeActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    addFlags(flags)
                }
                startActivity(intent)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        _layout = null
    }

    private fun validateEmailPassword(email: String, password: String) {
        if (email.isNotBlank()) {
            layout.tilEmail.error =
                if (isValidEmail(email)) "" else getString(R.string.invalid_email)
        }
        if (password.isNotBlank()) {
            layout.tilPassword.error =
                if (isValidPassword(password)) "" else getString(R.string.invalid_password)
        }
        layout.btnLogin.isEnabled = isValidEmail(email) && isValidPassword(password)
    }


    private fun isValidEmail(email: String): Boolean {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        val pattern = Pattern.compile(emailPattern)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun isValidPassword(password: String): Boolean {
        return password.length >= 8
    }

}