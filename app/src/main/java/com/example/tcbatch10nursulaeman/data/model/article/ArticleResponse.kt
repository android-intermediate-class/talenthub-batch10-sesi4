package com.example.tcbatch10nursulaeman.data.model.article

import kotlinx.serialization.Serializable

@Serializable
data class ArticleResponse(
    val articles: List<Article>?,
    val status: String?,
    val totalResults: Int?
)