package com.example.tcbatch10nursulaeman.utils.extension

fun <T> List<T>?.toArrayList(): ArrayList<T> {
    val list: ArrayList<T> = arrayListOf()
    this?.forEach { item ->
        list.add(item)
    }
    return list
}