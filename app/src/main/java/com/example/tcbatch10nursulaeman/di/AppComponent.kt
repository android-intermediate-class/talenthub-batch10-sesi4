package com.example.tcbatch10nursulaeman.di

import com.example.tcbatch10nursulaeman.presentation.detail.view.DetailActivity
import com.example.tcbatch10nursulaeman.presentation.favorite.view.FavoriteActivity
import com.example.tcbatch10nursulaeman.presentation.home.view.HomeFragment
import com.example.tcbatch10nursulaeman.presentation.login.view.LoginActivity
import com.example.tcbatch10nursulaeman.presentation.splash.view.SplashActivity
import com.example.tcbatch10nursulaeman.utils.viewmodelfactory.ViewModelModule
import dagger.Component

@Component(modules = [AppModule::class, ViewModelModule::class])
interface AppComponent {

    fun inject(splashActivity: SplashActivity)

    fun inject(loginActivity: LoginActivity)

    fun inject(homeFragment: HomeFragment)

    fun inject(favoriteActivity: FavoriteActivity)

    fun inject(detailActivity: DetailActivity)

}